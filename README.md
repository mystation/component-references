## MyStation case "Sango"

|Name|Reference|
|-   |-        |
|Motherboard|Raspberry Pi 3b+ \| 4 x 1,4 GHz \| 1 Go RAM|
|Radio emitter (433Mhz)|433Mhz wireless RF transmitter|
|Radio receptor (433Mhz)|433Mhz wireless RF receiver|

## Modules

Not available